package main

import (
	"flag"
	"log"

	"gitlab.com/tty8747/zavodokonbot/internal/telegram"
)

func main() {
	tgClient = telegram.New(mustHost, mustToken())

	// fetcher = fetcher.New(tgClient)

	// processor = processor.New(tgClient)

	// consumer.Start(fetcher, processor)
}

func mustToken() string {
	token := flag.String("token", "", "access token to telegram bot")
	flag.Parse()

	if *token == "" {
		log.Fatal("token isn't specified")
	}
	return *token
}

func mustHost() string {
	host := flag.String("api host", "", "api host to telegram bot")
	flag.Parse()

	if *host == "" {
		log.Fatal("host isn't specified")
	}
	return *host
}
